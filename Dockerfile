FROM golang:1.20.2-buster

USER root
RUN mkdir -p /opt/app

COPY ./server.conf.toml /opt/app/config/server.conf.toml
COPY ./ /opt/app

RUN cd /opt/app/src && GOOS=linux GOARCH=amd64 go build -o ../build/log-system .

WORKDIR /opt/app

EXPOSE 3000

CMD ["/opt/app/build/log-system", "-c", "/opt/app/config/server.conf.toml"]
