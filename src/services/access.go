package services

import (
	"errors"
	"net/http"

	"gitlab.com/logging-system/config"
	httpErrors "gitlab.com/logging-system/errors/http"
	"gitlab.com/logging-system/helpers"
)

func CheckAccess(request *http.Request) *httpErrors.HttpError {
	appConfig := config.GetConfig()

	if !helpers.InArray(request.Header.Get("Authorization"), appConfig.Server.AccessKeys) {
		return &httpErrors.HttpError{
			RawError: errors.New("Access denied"),
			Code:     403,
			Message:  "Access Denied",
			Data: map[string]interface{}{
				"ip":        request.RemoteAddr,
				"accessKey": request.Header.Get("Authorization"),
			},
		}
	}

	return nil
}
