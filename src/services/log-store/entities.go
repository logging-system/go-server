package logstore

import "time"

type Filter struct {
	Level    int
	Service  string
	TimeFrom *time.Time
	TimeTo   *time.Time
}

type LoadLogsParameters struct {
	Page          int
	Limit         int
	SortField     string
	SortDirection int
	Collection    string
	Filter        Filter
}
