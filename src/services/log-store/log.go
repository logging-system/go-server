package logstore

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/go-playground/validator"
	"gitlab.com/logging-system/database"
	"gitlab.com/logging-system/database/entities"
	httpErrors "gitlab.com/logging-system/errors/http"
	"gitlab.com/logging-system/helpers"
	"gitlab.com/logging-system/services"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func CreateLog(request *http.Request) (string, *httpErrors.HttpError) {
	accessError := services.CheckAccess(request)

	if accessError != nil {
		return "", accessError
	}

	var createRequestData entities.LogEntity

	err := json.NewDecoder(request.Body).Decode(&createRequestData)

	if err != nil {
		return "", &httpErrors.HttpError{
			RawError: err,
			Code:     400,
			Message:  "Cannot decoded json",
		}
	}

	validate := validator.New()

	err = validate.Struct(createRequestData)

	if err != nil {
		return "", &httpErrors.HttpError{
			RawError: err,
			Code:     422,
			Message:  "ValidationError",
			Data: map[string]interface{}{
				"validationMessage": err.Error(),
			},
		}
	}

	mongo := database.GetConnection()
	currentYear := fmt.Sprintf("%d", time.Now().Year())

	collection := mongo.Database(database.DEFAULT_DB).Collection(currentYear)

	found := collection.FindOne(request.Context(), bson.M{"id": createRequestData.Id})

	var foundedLog entities.LogEntity

	found.Decode(&foundedLog)

	if foundedLog.Id != "" {
		return foundedLog.Id, nil
	}

	_, err = collection.InsertOne(request.Context(), createRequestData)

	if err != nil {
		return "", &httpErrors.HttpError{
			RawError: err,
			Code:     500,
			Message:  "Cannot save this log record",
		}
	}

	return createRequestData.Id, nil
}

func LoadLogs(request *http.Request) ([]entities.LogEntity, *httpErrors.HttpError) {
	accessError := services.CheckAccess(request)

	var documents []entities.LogEntity

	if accessError != nil {
		return documents, accessError
	}

	queryParameters := parseLoadLogsParameters(request)

	dbCollection := database.GetConnection().Database(database.DEFAULT_DB).Collection(queryParameters.Collection)

	skip := (queryParameters.Page - 1) * queryParameters.Limit

	filter := bson.M{}

	if queryParameters.Filter.Level != 0 {
		filter["level"] = queryParameters.Filter.Level
	}

	if queryParameters.Filter.Service != "" {
		filter["service"] = queryParameters.Filter.Service
	}

	if queryParameters.Filter.TimeFrom != nil {
		filter["timestamp"] = bson.M{"$gte": queryParameters.Filter.TimeFrom}
	}

	if queryParameters.Filter.TimeTo != nil {
		filter["timestamp"] = bson.M{"$lte": queryParameters.Filter.TimeTo}
	}

	sortOptions := options.Find().SetSort(bson.M{queryParameters.SortField: queryParameters.SortDirection})

	limitOptions := options.Find().SetLimit(int64(queryParameters.Limit)).SetSkip(int64(skip))

	result, err := dbCollection.Find(context.TODO(), filter, sortOptions, limitOptions)

	if err != nil {
		return documents, &httpErrors.HttpError{
			RawError: err,
			Code:     500,
			Message:  "Cannot run query to database",
		}
	}

	result.All(context.TODO(), &documents)

	return documents, nil
}

func parseLoadLogsParameters(request *http.Request) LoadLogsParameters {
	var parameters LoadLogsParameters

	page := request.URL.Query().Get("page")

	if page == "" {
		page = "1"
	}

	parameters.Page, _ = strconv.Atoi(page)

	limit := request.URL.Query().Get("limit")

	if limit == "" {
		limit = "15"
	}

	parameters.Limit, _ = strconv.Atoi(limit)

	if parameters.Limit > 100 {
		parameters.Limit = 100
	}

	sortField := request.URL.Query().Get("sortField")

	if sortField == "" {
		sortField = "timestamp"
	}

	parameters.SortField = sortField

	sortDirection := request.URL.Query().Get("sortDirection")

	if sortDirection == "" {
		sortDirection = "desc"
	}

	if sortDirection == "asc" {
		parameters.SortDirection = 1
	} else {
		parameters.SortDirection = -1
	}

	collection := request.URL.Query().Get("collection")

	if collection == "" {
		collection = fmt.Sprintf("%d", time.Now().Year())
	}

	parameters.Collection = collection

	level := request.URL.Query().Get("level")

	parameters.Filter.Level, _ = strconv.Atoi(level)
	parameters.Filter.Service = request.URL.Query().Get("service")
	parameters.Filter.TimeFrom = parseTimeFromQuery(request, "timeFrom")
	parameters.Filter.TimeTo = parseTimeFromQuery(request, "timeTo")

	return parameters
}

func parseTimeFromQuery(request *http.Request, key string) *time.Time {
	timeLayout := "2006-01-02T15:04:05.000Z"

	timeFromQuery := request.URL.Query().Get(key)

	if timeFromQuery == "" {
		return nil
	}

	parsedTime, err := time.Parse(timeLayout, timeFromQuery)

	if err != nil {
		helpers.InternalLog(entities.LOG_LEVEL_INFO, err.Error(), map[string]interface{}{
			"time": timeFromQuery,
		})
		return nil
	}

	return &parsedTime
}
