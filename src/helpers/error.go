package helpers

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/google/uuid"
	"gitlab.com/logging-system/database/entities"
)

func CheckInternalFatal(err error) {
	if err != nil {
		InternalLog(entities.LOG_LEVEL_FATAL, err.Error(), nil)
		os.Exit(1)
	}
}

func InternalLog(level int, message string, data map[string]interface{}) {
	id := uuid.New()

	jsonLog, _ := json.Marshal(entities.LogEntity{
		Id:        id.String(),
		Level:     level,
		Message:   message,
		Service:   "dastanaron-log-system",
		Timestamp: time.Now(),
		Data:      data,
	})
	fmt.Println(string(jsonLog))
}
