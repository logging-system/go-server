package database

import (
	"context"

	"gitlab.com/logging-system/config"
	"gitlab.com/logging-system/helpers"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	DEFAULT_DB = "logs"
)

var connection *mongo.Client

func connect() *mongo.Client {
	appConfig := config.GetConfig()

	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(appConfig.Database.Uri))
	helpers.CheckInternalFatal(err)

	return client
}

func GetConnection() *mongo.Client {
	if connection != nil {
		return connection
	}

	connection = connect()

	return connection
}
