package entities

import "time"

const (
	LOG_LEVEL_TRACE = 1
	LOG_LEVEL_DEBUG = 2
	LOG_LEVEL_INFO  = 3
	LOG_LEVEL_WARN  = 4
	LOG_LEVEL_ERROR = 5
	LOG_LEVEL_FATAL = 6
)

type LogEntity struct {
	Id        string                 `bson:"id" json:"id" validate:"required"`
	Level     int                    `bson:"level" json:"level" validate:"required,gte=1,lte=6"`
	Message   string                 `bson:"message" json:"message" validate:"required"`
	Service   string                 `bson:"service" json:"service" validate:"required"`
	Timestamp time.Time              `bson:"timestamp" json:"timestamp" validate:"required"`
	Data      map[string]interface{} `bson:"data" json:"data"`
}
