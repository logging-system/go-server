package main

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/logging-system/config"
	"gitlab.com/logging-system/controllers"
	"gitlab.com/logging-system/database"
	"gitlab.com/logging-system/helpers"
	"gitlab.com/logging-system/routes"
)

func initRoutes() *http.ServeMux {
	router := http.NewServeMux()

	router.HandleFunc("/", controllers.HandleIndex)

	routes.SetUpLogRouters(router)

	return router
}

func closeDbConnection() {
	mongo := database.GetConnection()
	err := mongo.Disconnect(context.TODO())
	helpers.CheckInternalFatal(err)
}

func main() {
	appConfig := config.GetConfig()
	defer closeDbConnection()

	router := initRoutes()

	err := http.ListenAndServe(fmt.Sprint(":", appConfig.Server.Port), router)

	helpers.CheckInternalFatal(err)
}
