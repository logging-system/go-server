package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/logging-system/database/entities"
	httpErrors "gitlab.com/logging-system/errors/http"
	"gitlab.com/logging-system/helpers"
)

func buildErrorResponse(httpError *httpErrors.HttpError, response http.ResponseWriter, statusCode int) {
	errorStructure := ErrorResponse{"error", map[string]interface{}{"message": httpError.Message}}

	if value, ok := httpError.Data["validationMessage"]; ok {
		errorStructure.Data["validationMessage"] = value
	}

	helpers.InternalLog(entities.LOG_LEVEL_INFO, httpError.RawError.Error(), httpError.Data)

	json, err := json.Marshal(errorStructure)
	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(statusCode)
	_, err = response.Write(json)

	if err != nil {
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}
}

func buildResponse(response http.ResponseWriter, data Response, statusCode int) {
	if statusCode == 0 {
		statusCode = 200
	}

	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(statusCode)
	json.NewEncoder(response).Encode(data)
}

type ErrorResponse struct {
	Status string                 `json:"status"`
	Data   map[string]interface{} `json:"data"`
}

type Response struct {
	Status string      `json:"status"`
	Data   interface{} `json:"data"`
}
