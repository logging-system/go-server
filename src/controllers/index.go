package controllers

import (
	"net/http"
)

type ServerMessage struct {
	Message string `json:"message"`
}

func HandleIndex(response http.ResponseWriter, request *http.Request) {
	buildResponse(response, Response{
		Status: "ok",
		Data: ServerMessage{
			Message: "Server is ok",
		},
	}, 200)
}
