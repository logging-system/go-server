package controllers

import (
	"net/http"

	logstore "gitlab.com/logging-system/services/log-store"
)

func HandleLogCreate(response http.ResponseWriter, request *http.Request) {
	id, err := logstore.CreateLog(request)

	if err != nil {
		buildErrorResponse(err, response, err.Code)
		return
	}

	buildResponse(response, Response{
		Status: "ok",
		Data: map[string]string{
			"message": "successfull created",
			"id":      id,
		},
	}, 201)
}

func HandleLogList(response http.ResponseWriter, request *http.Request) {
	logList, err := logstore.LoadLogs(request)

	if err != nil {
		buildErrorResponse(err, response, err.Code)
		return
	}

	buildResponse(response, Response{
		Status: "ok",
		Data:   logList,
	}, 200)
}
