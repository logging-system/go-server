package http

type HttpError struct {
	RawError error
	Code     int
	Message  string
	Data     map[string]interface{}
}
