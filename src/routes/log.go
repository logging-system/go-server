package routes

import (
	"net/http"

	"gitlab.com/logging-system/controllers"
)

func SetUpLogRouters(router *http.ServeMux) {
	router.HandleFunc("/api/log/create", controllers.HandleLogCreate)
	router.HandleFunc("/api/log/load", controllers.HandleLogList)
}
