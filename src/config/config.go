package config

import (
	"flag"

	"github.com/pelletier/go-toml"
	"gitlab.com/logging-system/helpers"
)

type AppConfig struct {
	Database struct {
		Uri string `toml:"uri"`
	} `toml:"database"`
	Server struct {
		Port       string   `toml:"port"`
		AccessKeys []string `toml:"accessKeys"`
	} `toml:"server"`
}

var appConfig *AppConfig

func GetConfig() *AppConfig {
	if appConfig != nil {
		return appConfig
	}

	var pathToConfig string

	var config AppConfig

	flag.StringVar(&pathToConfig, "c", "", "[Required] Config file path")
	flag.Parse()

	tomlFile, err := toml.LoadFile(pathToConfig)
	helpers.CheckInternalFatal(err)

	err = tomlFile.Unmarshal(&config)
	helpers.CheckInternalFatal(err)

	appConfig = &config

	return appConfig
}
